import logging

from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render, redirect

from market.forms import WallpaperForm
from market.models import Wallpaper


logger = logging.getLogger('MarketView')


@permission_required(perm='market.view_wallpaper')
def list(request):
    #logger.warning(request.user.groups.all()[0].permissions.all())
    logger.warning(request.user.get_group_permissions())
    wallpapers = Wallpaper.objects.all()
    logger.warning(f'{request.user.username} requested wallpapers')
    logger.warning(f'{len(wallpapers)} wallpapers found for {request.user.username}')
    return render(request,
                  'market/wallpapers.html',
                  context={'wallpapers': wallpapers, 'injected_text': '<script>alert("FAA")</script>'})


@login_required
def item(request, item_id):
    wallpaper = Wallpaper.objects.get(slug=item_id)
    tags = wallpaper.tags.all()
    return render(request,
                  'market/wallpaper.html',
                  context={'wallpaper': wallpaper, 'tags': tags})


@staff_member_required
def add(request):
    if request.method == 'POST':
        form = WallpaperForm(request.POST, request.FILES)
        if form.is_valid():
            new_wallpaper = form.save(commit=False)
            new_wallpaper.creator = request.user
            new_wallpaper.save()
            form.save_m2m()
            return redirect('market:item', item_id=new_wallpaper.slug)
        return render(request, 'market/new_wallpaper.html', context={'form': form})
    form = WallpaperForm()
    return render(request, 'market/new_wallpaper.html', context={'form': form})
