from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.shortcuts import render, redirect


# Create your views here.
from user.forms import ExtendedUserCreationForm


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            login(request, form.get_user())
            if 'next' in request.GET:
                return redirect(request.GET.get('next'))
            return redirect('market:list')
        return render(request, 'user/login.html', context={'form': form})
    form = AuthenticationForm()
    return render(request, 'user/login.html', context={'form': form})


def logout_view(request):
    logout(request)
    return redirect('user:login')


def signup_view(request):
    if request.method == 'POST':
        form = ExtendedUserCreationForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('user:login')
        return render(request, 'user/signin.html', context={'form': form})
    form = ExtendedUserCreationForm()
    return render(request, 'user/signin.html', context={'form': form})
